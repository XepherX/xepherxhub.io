function Worldmap(canvas){
			

			this.width;
			this.height;

			this.noisegen = new SimplexNoise(
				{
					random: Math.random
				}
			);
			this.noisemap = []

			this.updateCanvas(canvas);
		}

		Worldmap.prototype.init = function(){
			this.noisemap = this.noisegen.octavedNoise(this.width, this.height, 9,0.7,0.00050);
			console.log(this.noisemap);
		};
		Worldmap.prototype.updateCanvas = function(canvas){
			console.log(canvas.width)
			this.width = canvas.width
			this.height = canvas.height
			this.canvas = canvas.getContext("2d");
			this.init()
		}
		Worldmap.prototype.drawWorldmap = function(){

			
			this.id = this.canvas.createImageData(this.width,this.height); // only do this once per page
			
			for (var y = 0; y < this.height ; y++) {
				
				for(var x = 0; x < this.width ; x++) {
					
					
					var pixel = this.noisemap[x][y];
					var rgb = {};
					if (pixel > 190) // if the generated height is over 180, we want ocean. for some reason, the noise generator generates stuff ass-backwards.
					{
						var tmpPixel =  pixel - 70; // some arithmagic to make the blue fancier. this failed pretty hard.
						if(tmpPixel > 255){tmpPixel = 255}
						
						rgb.r =  20 + tmpPixel / 8 - 255;
						rgb.g =  20 + tmpPixel / 8 - 255;
						rgb.b =  100 + Math.abs(tmpPixel - 255);
					}
					
					if(pixel > 180 & pixel <= 190) // sand
					{
						rgb.r = 224;
						rgb.g = 213;
						rgb.b = 0;
					}
					
					
					if(pixel > 90 && pixel <= 180) // land.
					{
						rgb.r = 0;
						rgb.g = Math.abs(255 - pixel); // arithmagic to have fancy green. didn't fail as hard.
						rgb.b = 0;
					}
					
					if(pixel <= 90) // mountains.
					{
						rgb.r = Math.abs(255 - pixel);
						rgb.g = rgb.r;
						rgb.b = rgb.r;
					}
					
					this.setPixel(this.id,x,y,rgb.r, rgb.g, rgb.b,255)

				}
				

			}

			this.canvas.putImageData(this.id,0,0)

		};



		Worldmap.prototype.setPixel = function (imageData, x, y, r, g, b, a) {
		    var index = (x + y * imageData.width) * 4;
		    imageData.data[index+0] = r;
		    imageData.data[index+1] = g;
		    imageData.data[index+2] = b;
		    imageData.data[index+3] = a;
		};
